﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;

namespace School.Models
{
    public class NewsPost
    {
        public int Id { get; set; }

        [CustomRequired]
        [DataType(DataType.Text)]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [CustomRequired]
        [DataType(DataType.Text)]
        [Display(Name = "Содержимое")]
        public string Content { get; set; }

        [CustomRequired]
        [DataType(DataType.Date)]
        [Display(Name = "Добавлено")]
        public DateTime PostedDate { get; set; }
        [Display(Name = "Id автора")]
        public string AuthorId { get; set; }
        public SchoolTeacher Author { get; set; }

    }
}

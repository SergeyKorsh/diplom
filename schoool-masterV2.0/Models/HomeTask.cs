﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;

namespace School.Models
{
    public class HomeTask
    {
        public int Id { get; set; }

        [CustomRequired]
        [Display(Name = "Предмет")]
        public string Subject { get; set; }

        [CustomRequired]
        [Display(Name = "Задание")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        [Display(Name = "Крайний срок")]
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }

        public int ClassId { get; set; }
        public SchoolClass Class { get; set; }
    }
}

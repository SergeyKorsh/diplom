﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;

namespace School.Models
{
    public class SchoolMark
    {
        public int Id { get; set; }

        [CustomRequired]
        [Display(Name = "Оценка")]
        public int Value { get; set; }

        [CustomRequired]
        [Display(Name = "Предмет")]
        public string Subject { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Дата")]
        public DateTime LessonDate { get; set; }

        [Display(Name = "Номер урока")]
        public int LessonOrderNumber { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        [Display(Name = "Ученик")]
        public int PupilId { get; set; }
        public SchoolPupil Pupil { get; set; }
    }
}

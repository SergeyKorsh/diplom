﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace School.ViewModels.HomeTasks
{
    public class EditHomeTasksModel
    {
        public int Id { get; set; }
        [Display(Name = "Предмет")]
        public string Subject { get; set; }

        [Display(Name = "Задание")]
        public string Content { get; set; }

        [Display(Name = "Крайний срок")]
        public DateTime Deadline { get; set; }
        [Display(Name = "ClassId")]
        public int ClassId { get; set; }
        public IEnumerable<SelectListItem> Tasks { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;
using School.Models;

namespace School.ViewModels.HomeTasks
{
    public class DeleteHomeTasksModel
    {
        public int Id { get; set; }
        [Display(Name = "Предмет")]
        public string Subject { get; set; }

        [Display(Name = "Задание")]
        public string Content { get; set; }

        [Display(Name = "Крайний срок")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Deadline { get; set; }
        [Display(Name = "ClassId")]
        public int ClassId { get; set; }
    }
}

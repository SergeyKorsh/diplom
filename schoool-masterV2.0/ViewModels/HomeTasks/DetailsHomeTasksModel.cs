﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using School.Attributes;


namespace School.ViewModels.HomeTasks
{
    public class DetailsHomeTasksModel
    {
        [Display(Name = "Предмет")]
        public string Subject { get; set; }
       
        [Display(Name = "Задание")]
        public string Content { get; set; }

        [Display(Name = "Крайний срок")]
        public DateTime Deadline { get; set; }
        [Display(Name = "ClassId")]
        public int ClassId { get; set; }

    }
}

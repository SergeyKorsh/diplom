﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using School.Attributes;

namespace School.ViewModels.HomeTasks
{
    public class CreateHomeTaskModel
    {
        [CustomRequired]
        [Display(Name = "Предмет")]
        public string Subject { get; set; }

        [CustomRequired]
        [Display(Name = "Задание")]
        public string Content { get; set; }

        [Display(Name = "Крайний срок")]

        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }
        public int ClassId { get; set; }
        public IEnumerable<SelectListItem> Tasks { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using School.Models;

namespace School.ViewModels.NewsPosts
{
    public class EditNewsPostModel
    {
        public int Id { get; set; }

        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        [Display(Name = "Содержимое")]
        public string Content { get; set; }
        [Display(Name = "PostDate")]
        public DateTime PostedDate { get; set; }
        [Display(Name = "AuthorId")]
        public string AuthorId { get; set; }
        public IEnumerable<SelectListItem> News { get; set; }

    }
}

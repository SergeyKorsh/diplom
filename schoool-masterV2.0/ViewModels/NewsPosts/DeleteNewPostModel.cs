﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.NewsPosts
{
    public class DeleteNewPostModel
    {//
        public int Id { get; set; }

        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        [Display(Name = "Содержимое")]
        public string Content { get; set; }
    }
}

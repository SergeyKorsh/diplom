﻿using School.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.NewsPosts
{
    public class CreateNewPostModel
    {//
        [CustomRequired]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [CustomRequired]
        [Display(Name = "Содержимое")]
        public string Content { get; set; }

        public DateTime PostedDate { get; set; }
        public string AuthorId { get; set; }


    }
}

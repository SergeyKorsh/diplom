﻿using Microsoft.AspNetCore.Mvc.Rendering;
using School.Attributes;
using School.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.NewsPosts
{
    public class DetailsNewsPostModel
    {
        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        [Display(Name = "Содержимое")]
        public string Content { get; set; }
        [Display(Name = "PostDate")]
        public DateTime PostedDate { get; set; }
        [Display(Name = "AuthorId")]
        public string AuthorId { get; set; }

    }
}

﻿using School.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.Home
{
    public class HomeModel
    {
            public int Id { get; set; }

            [Display(Name = "Заголовок")]
            public string Title { get; set; }

            [Display(Name = "Содержимое")]
            public string Content { get; set; }

            [Display(Name = "Добавлено")]
            public DateTime PostedDate { get; set; }
       
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }       
       
    }
}

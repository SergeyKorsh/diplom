﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;
using School.Models;
using School.ViewModels;

namespace School.ViewModels.SchoolClasses
{
    public class DetailsSchoolClassesModel
    {
        public int Id { get; set; }
        [CustomRequired]
        [Display(Name = "Номер класса")]
        public string Name { get; set; }
        public List<IndexPupilModel> Pupils { get; set; }
        public IEnumerable<IGrouping<DateTime, HomeTask>> Tasks { get; set; }
    }
}

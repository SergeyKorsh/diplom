﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using School.Models;

namespace School.ViewModels.SchoolClasses
{
    public class EditSchoolClassModel
    {
        public int Id { get; set; }
        [CustomRequired]
        [Display(Name = "Номер класса")]
        public string Name { get; set; }
    }
}

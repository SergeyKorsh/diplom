﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.SchoolClasses
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}

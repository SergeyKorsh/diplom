﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using School.Attributes;

namespace School.ViewModels.SchoolClasses
{//
    public class CreateSchoolClassesModel
    {
        [CustomRequired]
        [Display(Name="Номер класса")]
        public string Name { get; set; }
    }
}

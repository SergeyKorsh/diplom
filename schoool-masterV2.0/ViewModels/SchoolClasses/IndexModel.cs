﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using School.ViewModels;
using School.Models;


namespace School.ViewModels.SchoolClasses
{
    public class IndexModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<SchoolPupil> Pupils { get; set; }

    }
}

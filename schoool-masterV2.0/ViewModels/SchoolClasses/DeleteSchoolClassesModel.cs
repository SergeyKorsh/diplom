﻿using Microsoft.AspNetCore.Mvc.Rendering;
using School.Attributes;
using School.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.SchoolClasses
{
    public class DeleteSchoolClassesModel
    {//
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

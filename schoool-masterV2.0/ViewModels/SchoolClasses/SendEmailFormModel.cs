﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.SchoolClasses
{
    public class SendEmailFormModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

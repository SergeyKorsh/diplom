﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;

namespace School.ViewModels.SchoolPupils
{//
    public class CreatePeopleModel
    {
        [CustomRequired]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [CustomRequired]
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [CustomRequired]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [CustomRequired]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [CustomRequired]
        [Display(Name = "Адрес")]
        public string Address { get; set; }
        [Display(Name = "Название класса")]
        public int ClassId { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; }

        public CreatePeopleModel()
        {
            Classes = new List<SelectListItem>();
        }
    }
}

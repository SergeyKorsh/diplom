﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.SchoolPupils
{
    public class MarksModel
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public string Subject { get; set; }
        public DateTime LessonDate { get; set; }
    }
}

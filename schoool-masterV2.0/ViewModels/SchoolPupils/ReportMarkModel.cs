﻿using School.Models;
using System;
using System.Collections.Generic;

namespace School.ViewModels.SchoolPupils
{
    public class ReportMarkModel
    {
        public double Value { get; set; }
        public string Subject { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace School.ViewModels.SchoolPupils
{
    public class ReportModel
    {
        public string ClassName { get; set; }
        public string FullName { get; set; }
        public string Period { get; set; }
        public string CurrenDate { get; set; }

        public ICollection<ReportMarkModel> Marks { get; set; }

        public ReportModel()
        {
            Marks = new List<ReportMarkModel>();
        }
    }
}

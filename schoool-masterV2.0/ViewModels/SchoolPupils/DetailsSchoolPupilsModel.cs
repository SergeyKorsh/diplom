﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using School.Models;
using School.ViewModels;

namespace School.ViewModels.SchoolPupils
{
    public class DetailsSchoolPupilsModel
    {
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Адрес")]
        public string Address { get; set; }
        [Display(Name = "Название класса")]
        public string Name { get; set; }
        public int ClassId { get; set; }
        //public List<SchoolMark> Marks { get; set; }

         public List<MarksModel> Marks { get; set; }
        //public ICollection<MarksModel> Marks { get; set; }

        //public DetailsSchoolPupilsModel()
        //{
        //    Marks = new List<MarksModel>();
        //}

    }
}

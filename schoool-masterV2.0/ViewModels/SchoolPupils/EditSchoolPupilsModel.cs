﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using School.Models;

namespace School.ViewModels.SchoolPupils
{
    public class EditSchoolPupilsModel
    {
        public int Id { get; set; }
        [CustomRequired]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [CustomRequired]
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        [CustomRequired]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [CustomRequired]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [CustomRequired]
        [Display(Name = "Адрес")]
        public string Address { get; set; }
        [Display(Name = "Название класса")]
        public int ClassId { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; }
    }
}

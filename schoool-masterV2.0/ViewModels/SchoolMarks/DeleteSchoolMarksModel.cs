﻿using Microsoft.AspNetCore.Mvc.Rendering;
using School.Attributes;
using School.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace School.ViewModels.SchoolMarks
{
    public class DeleteSchoolMarksModel
    {
        public int Id { get; set; }
         public int Value { get; set; }
        [Display(Name = "Предмет")]
        public string Subject { get; set; }

        [Display(Name = "Дата")]
        public DateTime LessonDate { get; set; }

        [Display(Name = "Номер урока")]
        public int LessonOrderNumber { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }
        [Display(Name = "PupilId")]
        public int PupilId { get; set; }
       
    }
}

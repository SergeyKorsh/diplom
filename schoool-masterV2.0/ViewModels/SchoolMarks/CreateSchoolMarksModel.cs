﻿using School.Attributes;
using School.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace School.ViewModels.SchoolMarks
{
    public class CreateSchoolMarksModel
    {

        [CustomRequired]
        [Display(Name = "Оценка")]
        public int Value { get; set; }

        [CustomRequired]
        [Display(Name = "Предмет")]
        public string Subject { get; set; }

        [Display(Name = "Дата")]
        [DataType(DataType.Date)]
        public DateTime LessonDate { get; set; }

        [Display(Name = "Номер урока")]
        public int LessonOrderNumber { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }
        public int PupilId { get; set; }
    }
}

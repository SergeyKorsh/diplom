﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using School.Attributes;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using School.Models;

namespace School.ViewModels.SchoolMarks
{
    public class EditSchoolMarksModel
    {
        public int Id { get; set; }
        [CustomRequired]
        [Display(Name = "Оценка")]
        public int Value { get; set; }

        [CustomRequired]
        [Display(Name = "Предмет")]
        public string Subject { get; set; }

        [Display(Name = "Дата")]
        [DataType(DataType.Date)]
        public DateTime LessonDate { get; set; }

        [Display(Name = "Номер урока")]
        public int LessonOrderNumber { get; set; }
        public int PupilId { get; set; }
        public IEnumerable<SelectListItem> Marks { get; set; }
    }
}

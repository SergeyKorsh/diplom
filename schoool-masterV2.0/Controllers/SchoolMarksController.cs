﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using School.Data;
using School.Models;
using School.ViewModels.SchoolMarks;

namespace School.Controllers
{
    public class SchoolMarksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SchoolMarksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SchoolMarks
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Marks.Include(s => s.Pupil);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SchoolMarks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolMark = await _context.Marks
                .Include(s => s.Pupil)
                .FirstOrDefaultAsync(m => m.Id == id);
            DetailsSchoolMarksModel model = new DetailsSchoolMarksModel
            {
                Id = schoolMark.Id,
                Value = schoolMark.Value,
                Subject = schoolMark.Subject,
                LessonDate = schoolMark.LessonDate,
                LessonOrderNumber = schoolMark.LessonOrderNumber,
                PupilId = schoolMark.PupilId
            };
            if (schoolMark == null)
            {
                return NotFound();
            }

            return View(model);
        }

        // GET: SchoolMarks/Create
        public IActionResult Create(int id)
        {
            var model = new CreateSchoolMarksModel
            {
                PupilId = id
            };
            return View(model);
        }

        // POST: SchoolMarks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( CreateSchoolMarksModel model/*[Bind("")] */)
        {
            if (ModelState.IsValid)
            {
                var schoolMark = new SchoolMark {
                    LessonDate = model.LessonDate,
                    Value= model.Value,
                    Subject= model.Subject,
                    LessonOrderNumber= model.LessonOrderNumber,
                    PupilId= model.PupilId
                };
                _context.Add(schoolMark);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "SchoolPupils", new { id = model.PupilId});
            }
            return View(model);
        }

        // GET: SchoolMarks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolMark = await _context.Marks.FindAsync(id);
            if (schoolMark == null)
            {
                return NotFound();
            }
            var model = new EditSchoolMarksModel()
            {
                Id = schoolMark.Id,
                Value = schoolMark.Value,
                Subject = schoolMark.Subject,
                LessonDate = schoolMark.LessonDate,
                LessonOrderNumber = schoolMark.LessonOrderNumber,
                PupilId = schoolMark.PupilId
            };
            FillEditHomeTaskModel(model, schoolMark.PupilId);
            return View(model);
        }
        private void FillEditHomeTaskModel(EditSchoolMarksModel model, int pupilId)
        {

            model.Marks = new SelectList(_context.Pupils, "Id", "LastName", pupilId);
        }
        // POST: SchoolMarks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditSchoolMarksModel model)
        {
            var schoolMark = new SchoolMark
            {
                Id = model.Id,                
                Value=model.Value,
                Subject=model.Subject,
                LessonDate=model.LessonDate,
                LessonOrderNumber=model.LessonOrderNumber,
                PupilId=model.PupilId
            };
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schoolMark);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolMarkExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            FillEditHomeTaskModel(model, schoolMark.PupilId);
            return View(model);
        }

        // GET: SchoolMarks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolMark = await _context.Marks
                .Include(s => s.Pupil)
                .FirstOrDefaultAsync(m => m.Id == id);
           
            if (schoolMark == null)
            {
                return NotFound();
            }
 var model = new DeleteSchoolMarksModel()
            {
                Id = schoolMark.Id,
                Value = schoolMark.Value,
                Subject = schoolMark.Subject,
                LessonDate = schoolMark.LessonDate,
                LessonOrderNumber = schoolMark.LessonOrderNumber,
                PupilId = schoolMark.PupilId
            };
            return View(model);
        }

        // POST: SchoolMarks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var schoolMark = await _context.Marks.FindAsync(id);
            _context.Marks.Remove(schoolMark);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolMarkExists(int id)
        {
            return _context.Marks.Any(e => e.Id == id);
        }
    }
}

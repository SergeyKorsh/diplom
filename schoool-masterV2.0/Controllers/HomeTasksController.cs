﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using School.Data;
using School.Models;
using School.ViewModels.HomeTasks;

namespace School.Controllers
{
    public class HomeTasksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeTasksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HomeTasks
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Tasks.Include(h => h.Class);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: HomeTasks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var homeTask = await _context.Tasks
                .Include(h => h.Class)
                .FirstOrDefaultAsync(m => m.Id == id);           
            if (homeTask == null)
            {
                return NotFound();
            }
           var model = new DetailsHomeTasksModel()
            {
                Subject = homeTask.Subject,
                Content = homeTask.Content,
                Deadline = homeTask.Deadline,
                ClassId = homeTask.ClassId
            };
            return View(model);
        }

        // GET: HomeTasks/Create
        public IActionResult Create(int id)
        {
            var model = new CreateHomeTaskModel {
                ClassId = id
            };
           
            return View(model);
        }

        // POST: HomeTasks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateHomeTaskModel model )
        {
            if (ModelState.IsValid)
            {
                var homeTask = new HomeTask {
                    Subject = model.Subject,
                    Content=model.Content,
                    Deadline=model.Deadline,
                    ClassId=model.ClassId
                };
                _context.Add(homeTask);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "SchoolClasses", new { id = model.ClassId });
            }
            model.Tasks = new SelectList(_context.Classes, "Id", "Id", model.ClassId);
            return View(model);
        }



        // GET: HomeTasks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homeTask = await _context.Tasks.FindAsync(id);
            if (homeTask == null)
            {
                return NotFound();
            }
            var model = new EditHomeTasksModel()            
            {
                Id = homeTask.Id,
                Subject = homeTask.Subject,
                Content = homeTask.Content,
                Deadline = homeTask.Deadline,
                ClassId = homeTask.ClassId
               };
            FillEditHomeTaskModel(model, homeTask.ClassId);
            return View(model);
        }

        private void FillEditHomeTaskModel(EditHomeTasksModel model, int classId) 
        {
            
            model.Tasks = new SelectList(_context.Classes, "Id", "Name", classId);
        }

        // POST: HomeTasks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditHomeTasksModel model)
        {
            var homeTask = new HomeTask {
            Id = model.Id,
            Subject=model.Subject,
            Content=model.Content,
            Deadline=model.Deadline,
            ClassId=model.ClassId
            };
            if (id != model.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(homeTask);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HomeTaskExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            FillEditHomeTaskModel(model, homeTask.ClassId);
            return View(model);
        }

        // GET: HomeTasks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var homeTask = await _context.Tasks
                .Include(h => h.Class)
                .FirstOrDefaultAsync(m => m.Id == id);
            
            if (homeTask == null)
            {
                return NotFound();
            }

             DeleteHomeTasksModel model = new DeleteHomeTasksModel
            {
                Id = homeTask.Id,
                Subject = homeTask.Subject,
                Content = homeTask.Content,
                Deadline = homeTask.Deadline,
                ClassId = homeTask.ClassId
            };
            return View(model);
        }

        // POST: HomeTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var homeTask = await _context.Tasks.FindAsync(id);
            _context.Tasks.Remove(homeTask);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HomeTaskExists(int id)
        {
            return _context.Tasks.Any(e => e.Id == id);
        }
    }
}

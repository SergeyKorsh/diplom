﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using School.Data;
using School.Models;
using School.ViewModels.Home;

namespace School.Controllers
{
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _context;
        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            _context.Posts.Include(p => p.Author);
            _context.Teachers.Load();
            var homeModels = _context.Posts.OrderByDescending(p => p.PostedDate)
            .Select(p => new HomeModel()
            {
                Id=p.Id,
                Content = p.Content,
                PostedDate = p.PostedDate,
                Title = p.Title,
                LastName=p.Author.LastName,
                MiddleName=p.Author.MiddleName,
                FirstName=p.Author.FirstName                

            });
            return View(homeModels);          
        }
    }
}

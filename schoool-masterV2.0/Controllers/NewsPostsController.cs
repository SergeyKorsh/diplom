﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using School.Data;
using School.Models;
using School.ViewModels.NewsPosts;
using AutoMapper;

namespace School.Controllers
{
    public class NewsPostsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<SchoolTeacher> _userManager;

        public NewsPostsController(ApplicationDbContext context,
                                   UserManager<SchoolTeacher> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: NewsPosts
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Posts.Include(n => n.Author);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: NewsPosts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
             {
                 return NotFound();
             }
            var configur = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<NewsPost, DetailsNewsPostModel>();
            });
            var newsPost = await _context.Posts
                 .Include(n => n.Author)
                 .FirstOrDefaultAsync(m => m.Id == id);
            var mapper = new Mapper(configur);
           
            if (newsPost == null)
             {
                 return NotFound();
             }
            DetailsNewsPostModel user = mapper.Map<NewsPost, DetailsNewsPostModel>(newsPost);
                  
            return View(user);
        }

        // GET: NewsPosts/Create
        public IActionResult Create()
        {           
            return View();
        }

        // POST: NewsPosts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateNewPostModel model)
        {
            if (ModelState.IsValid)
            {
                var userDetails = (await _userManager.FindByNameAsync(User.Identity.Name)).Id;
                var config = new MapperConfiguration(cfg => cfg.CreateMap<CreateNewPostModel, NewsPost>()
                    .ForMember("PostedDate", opt => opt.MapFrom(c => DateTime.Now))
                    .ForMember("AuthorId", opt => opt.MapFrom(src => userDetails)));
                
                var mapper = new Mapper(config);
                // Выполняем сопоставление
                NewsPost user = mapper.Map<CreateNewPostModel, NewsPost>(model);              
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }
       
        // GET: NewsPosts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsPost = await _context.Posts.FindAsync(id);
            if (newsPost == null)
            {
                return NotFound();
            }
            var model = new EditNewsPostModel()
            {
                Id = newsPost.Id,
                Title = newsPost.Title,
                Content = newsPost.Content,
                PostedDate = newsPost.PostedDate,
                AuthorId = newsPost.AuthorId
            };
            FillEditNewsPostModel(model, newsPost.AuthorId);
            return View(model);
        }
        private void FillEditNewsPostModel(EditNewsPostModel model, string authorId)
        {

            model.News = new SelectList(_context.Teachers, "Id", "LastName", authorId);
        }
        // POST: NewsPosts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
          public async Task<IActionResult> Edit(int id, EditNewsPostModel model)
          {
              var newsPost = new NewsPost
              {
                  Id = model.Id,
                  Title = model.Title,
                  Content = model.Content,
                  PostedDate = model.PostedDate,
                  AuthorId = model.AuthorId
              };
              if (id != model.Id)
              {
                  return NotFound();
              }
              if (ModelState.IsValid)
              {
                  try
                  {
                      _context.Update(newsPost);
                      await _context.SaveChangesAsync();
                  }
                  catch (DbUpdateConcurrencyException)
                  {
                      if (!NewsPostExists(model.Id))
                      {
                          return NotFound();
                      }
                      else
                      {
                          throw;
                      }
                  }
                  return RedirectToAction(nameof(Index));
              }
            FillEditNewsPostModel(model, newsPost.AuthorId);
            return View(model);
          }

        // GET: NewsPosts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newsPost = await _context.Posts
                .Include(n => n.Author)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (newsPost == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(newsPost);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Home");
        }

        private bool NewsPostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }
    }
}

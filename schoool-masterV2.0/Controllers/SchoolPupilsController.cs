﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using School.Data;
using School.Models;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using MailKit.Net.Smtp;
using MimeKit;
using School.ViewModels.SchoolPupils;

namespace School.Controllers
{
    public class SchoolPupilsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SchoolPupilsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SchoolPupils
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Pupils.Include(s => s.Class);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SchoolPupils/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolPupil = await _context.Pupils
                .Include(s => s.Class)
                .FirstOrDefaultAsync(m => m.Id == id);           

            if (schoolPupil == null)
            {
                return NotFound();
            }
            _context.Classes.Load();
            _context.Marks.Load();
            DetailsSchoolPupilsModel model = new DetailsSchoolPupilsModel {
                Id = schoolPupil.Id,
                FirstName = schoolPupil.FirstName,
                MiddleName = schoolPupil.MiddleName,
                LastName = schoolPupil.LastName,
                Email = schoolPupil.Email,
                Address = schoolPupil.Address,
                ClassId = schoolPupil.ClassId,
                Name=schoolPupil.Class.Name,
                Marks = _context.Marks.Where(o => o.PupilId == id)
            .Select(o => new MarksModel
            {
                Id=o.Id,
                LessonDate = o.LessonDate,
                Subject = o.Subject,
                Value = o.Value
            }).ToList()
                //schoolPupil.Marks
            };

            return View(model);
        }

        // GET: SchoolPupils/Create
        public IActionResult Create()
        {
            var model = new CreatePeopleModel
            {
                Classes = new SelectList(_context.Classes, "Id", "Name")
            };

            return View(model);
        }

        // POST: SchoolPupils/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreatePeopleModel model)
        {
            if (ModelState.IsValid)
            {
                //TODO: Mapper
                var schoolPupil = new SchoolPupil
                {
                    FirstName = model.FirstName,
                    MiddleName = model.MiddleName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Address = model.Address,
                    ClassId = model.ClassId
                };

                //TODO: Business Layer
                _context.Add(schoolPupil);
                await _context.SaveChangesAsync();


                return RedirectToAction(nameof(Index));
            }

            model.Classes = new SelectList(_context.Classes, "Id", "Name");
            return View(model);
        }

        // GET: SchoolPupils/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolPupil = await _context.Pupils.FindAsync(id);
           
            if (schoolPupil == null)
            {
                return NotFound();
            }
            var model = new EditSchoolPupilsModel()
            {
                Id = schoolPupil.Id,
                FirstName = schoolPupil.FirstName,
                MiddleName = schoolPupil.MiddleName,
                LastName = schoolPupil.LastName,
                Email = schoolPupil.Email,
                Address = schoolPupil.Address,
                ClassId = schoolPupil.ClassId
            };
            FillEditSchoolPupilsModel(model, schoolPupil.ClassId);
            return View(model);
        }
        private void FillEditSchoolPupilsModel(EditSchoolPupilsModel model, int classId)
        {

            model.Classes = new SelectList(_context.Classes, "Id", "Name", classId);
        }
        // POST: SchoolPupils/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditSchoolPupilsModel model)
        {
            var schoolPupil = new SchoolPupil {
                Id=model.Id,
                FirstName=model.FirstName,
                MiddleName=model.MiddleName,
                LastName=model.LastName,
                Email=model.Email,
                Address=model.Address,
                ClassId=model.ClassId
            };
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schoolPupil);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolPupilExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            FillEditSchoolPupilsModel(model, schoolPupil.ClassId);
            return View(model);
        }

        // GET: SchoolPupils/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolPupil = await _context.Pupils
                .Include(s => s.Class)
                .FirstOrDefaultAsync(m => m.Id == id);
           
            if (schoolPupil == null)
            {
                return NotFound();
            }
 var model = new DeleteSchoolPupilsModel()
            {
                Id = schoolPupil.Id,
                FirstName = schoolPupil.FirstName,
                MiddleName = schoolPupil.MiddleName,
                LastName = schoolPupil.LastName,
                Email = schoolPupil.Email,
                Address = schoolPupil.Address,
                ClassId = schoolPupil.ClassId
            };
            return View(schoolPupil);
        }

        // POST: SchoolPupils/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var schoolPupil = await _context.Pupils.FindAsync(id);
            _context.Pupils.Remove(schoolPupil);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolPupilExists(int id)
        {
            return _context.Pupils.Any(e => e.Id == id);
        }

        public IActionResult Report(int? id, DateTime begin, DateTime end)
        {
            SchoolPupil pupil = _context.Pupils.Include(o=>o.Class).Where(p => p.Id == id).FirstOrDefault();

            if (pupil == null)
            {
                return NotFound();
            }

            var model = new ReportModel {
                Period = string.Format("{0} по {1}", begin.ToShortDateString(), end.ToString("dd-MM-yyyy")),
                FullName = string.Format("{0} {1} {2}", pupil.LastName, pupil.FirstName, pupil.MiddleName),
                ClassName = pupil.Class.Name,
                CurrenDate = DateTime.Now.ToShortDateString(),
                Marks = _context.Marks.Where(o =>
                                o.Pupil.Id == id
                                &&
                                o.LessonDate >= begin
                                &&
                                o.LessonDate <= end)
                            .GroupBy(o => o.Subject)
                            .Select(o => new ReportMarkModel {
                                 Subject = o.Key,
                                Value = o.Average(g => g.Value)
                            }).ToList()
            };

            return View(model);
        }

        public IActionResult Pdf(int? id, DateTime begin, DateTime end)
        {
            HtmlToPdfConverter converter = new HtmlToPdfConverter();
            WebKitConverterSettings settings = new WebKitConverterSettings
            {
                WebKitPath = @"QtBinariesWindows"
            };
            converter.ConverterSettings = settings;

            settings.HttpPostFields.Add("id", id.ToString());
            settings.HttpPostFields.Add("begin", begin.GetDateTimeFormats()[0]);
            settings.HttpPostFields.Add("end", end.GetDateTimeFormats()[0]);

            PdfDocument pdfDocument = converter.Convert("https://localhost:44394/SchoolPupils/Report");

            MemoryStream ms = new MemoryStream();
            pdfDocument.Save(ms);

            ms.Position = 0;

            pdfDocument.Close(true);

            FileStreamResult result = new FileStreamResult(ms, "application/pdf")
            {
                FileDownloadName = "Report.pdf"
            };

            return result;
        }

        [HttpGet]
        public IActionResult SendEmail(int? id)
        {
            _context.Pupils.Load();
            return View("SendEmailForm", _context.Pupils.Where(p => p.Id == id).FirstOrDefault());
        }

        [HttpPost]
        public IActionResult SendEmail(int? id, string msg, string subject)
        {
            _context.Pupils.Load();
            SchoolPupil pupil = _context.Pupils.Where(p => p.Id == id).FirstOrDefault();

            MimeMessage message = new MimeMessage();

            MailboxAddress from = new MailboxAddress("Школа д. Зимница",
            "korsunovs368@gmail.com");
            message.From.Add(from);

            MailboxAddress to = new MailboxAddress(pupil.LastName + " " + pupil.FirstName,
            pupil.Email);
            message.To.Add(to);

            message.Subject = subject;

            BodyBuilder bodyBuilder = new BodyBuilder
            {
                TextBody = msg
            };

            message.Body = bodyBuilder.ToMessageBody();

            SmtpClient client = new SmtpClient();
            client.Connect("smtp.gmail.com", 465, true);
            client.Authenticate("korsunovs368@gmail.com", "sergoo555");

            client.Send(message);
            client.Disconnect(true);
            client.Dispose();

            return RedirectToAction("Details", "SchoolPupils", new{id});
        }
    }
}

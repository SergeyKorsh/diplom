﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using School.Data;
using School.Models;
using School.ViewModels.SchoolClasses;

namespace School.Controllers
{
    public class SchoolClassesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SchoolClassesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SchoolClasses
        public IActionResult Index()
        {
            _context.Pupils.Load();
            var model = _context.Classes.OrderBy(p => p.Id)
           .Select(p => new IndexModel()
           {
               Id = p.Id,
               Name = p.Name,
               Pupils= p.Pupils
               //Pupils = _context.Classes
               //             .Select(o => new IndexNameModel
               //             {
               //                 Name = o.Name                                
               //             }).ToList()
           });

            return View(model);
        }

        // GET: SchoolClasses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClass = await _context.Classes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolClass == null)
            {
                return NotFound();
            }
 
            _context.Pupils.Load();
            _context.Tasks.Load();
DetailsSchoolClassesModel model = new DetailsSchoolClassesModel
            {
                Id = schoolClass.Id,
                Name = schoolClass.Name,
                Pupils= _context.Pupils.Where(o=> o.ClassId==id)
                .Select(o=> new IndexPupilModel {
                    Id=o.Id,
                FirstName=o.FirstName,
                MiddleName=o.MiddleName,
                LastName=o.LastName
                }).ToList(),
    //Tasks = (IEnumerable<IGrouping<DateTime, TaskModel>>)_context.Tasks.Where(o=> o.ClassId==id 
    //&& o.Deadline >= DateTime.Now)
    //.GroupBy(o=> o.Deadline)

    Tasks = from task in _context.Tasks.AsEnumerable()
    where task.ClassId == id
          && task.Deadline >= DateTime.Now
    group task by task.Deadline
};
              return View(model);
        }

        // GET: SchoolClasses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SchoolClasses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateSchoolClassesModel model)
        {
            if (ModelState.IsValid)
            {
                var schoolClass = new SchoolClass
                {
                    Name = model.Name
                };
                _context.Add(schoolClass);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: SchoolClasses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClass = await _context.Classes.FindAsync(id);
            if (schoolClass == null)
            {
                return NotFound();
            }
            var model = new EditSchoolClassModel()
            {
                Id = schoolClass.Id,
                Name = schoolClass.Name,
            };
            return View(model);
        }

        // POST: SchoolClasses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, EditSchoolClassModel model)
        {
            var schoolClass = new SchoolClass
            {
                Id = model.Id,
                Name = model.Name
            };
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schoolClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SchoolClassExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: SchoolClasses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClass = await _context.Classes
                .FirstOrDefaultAsync(m => m.Id == id);
           
            if (schoolClass == null)
            {
                return NotFound();
            }
            var model = new DeleteSchoolClassesModel
            {
                Id = schoolClass.Id,
                Name = schoolClass.Name
            };
            return View(model);
        }

        // POST: SchoolClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var schoolClass = await _context.Classes.FindAsync(id);
            _context.Classes.Remove(schoolClass);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SchoolClassExists(int id)
        {
            return _context.Classes.Any(e => e.Id == id);
        }

        [HttpGet]
        public IActionResult SendEmail(int? id)
        {
            _context.Pupils.Load();
           var schoolClass = _context.Classes.Where(p => p.Id == id).FirstOrDefault();
            var model = new SendEmailFormModel() 
            { 
            Id = schoolClass.Id,
            Name=schoolClass.Name
            };
            return View("SendEmailForm", model);
        }

        [HttpPost]
        public IActionResult SendEmail(int? id, string msg, string subject)
        {
            _context.Pupils.Load();
            MimeMessage message = new MimeMessage();

            MailboxAddress from = new MailboxAddress("Школа д. Зимница", "korsunovs368@gmail.com");
            message.From.Add(from);

            SchoolClass schoolClass = _context.Classes.Where(cl => cl.Id == id).FirstOrDefault();
            foreach(var pupil in schoolClass.Pupils)
            {
                MailboxAddress to = new MailboxAddress(pupil.LastName + " " + pupil.FirstName, pupil.Email);
                
                message.To.Add(to);
                message.Subject = subject;

                BodyBuilder bodyBuilder = new BodyBuilder
                {
                    TextBody = msg
                };

                message.Body = bodyBuilder.ToMessageBody();

                SmtpClient client = new SmtpClient();
                client.Connect("smtp.gmail.com", 465, true);
                client.Authenticate("korsunovs368@gmail.com", "sergoo555");

                client.Send(message);
                client.Disconnect(true);
                client.Dispose();
            }
            return RedirectToAction("Details", "SchoolClasses", new { id });
        }
    }
}
